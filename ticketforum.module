<?php
/**
 * @file
 *   TicketForum module file.
 */

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ticketforum\EntityAccessHooks;
use Drupal\ticketforum\FormHooks;

/**
 * Implements hook_forum_inheritance_field_configuration().
 */
function ticketforum_forum_inheritance_field_configuration() {
  return ['ticketforum' => [
    'node_bundle' => 'ticketforum',
    'term_bundle' => 'ticketforums',
    'term_reference' => 'field_taxonomy_ticketforums',
    'query_parameter' => 'ticketforum_id',
  ]];
}

/**
 * Implements hook_entity_prepare_form().
 *
 * @throws \Drupal\Core\TypedData\Exception\MissingDataException
 * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
 */
function ticketforum_entity_prepare_form(EntityInterface $entity, $operation, FormStateInterface $form_state) {
  FormHooks::hookEntityPrepareForm($entity, $operation, $form_state);
}

/**
 * Implements hook_form_alter().
 */
function ticketforum_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  FormHooks::hookFormAlter($form, $form_state, $form_id);
}

/**
 * Implements hook_field_widget_form_alter().
 */
function ticketforum_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  FormHooks::hookFieldWidgetFormAlter($element, $form_state, $context);
}

/**
 * Implements hook_menu_local_tasks_alter().
 *
 * @throws \Drupal\Core\TypedData\Exception\MissingDataException
 */
function ticketforum_menu_local_tasks_alter(&$data, $route_name, \Drupal\Core\Cache\RefinableCacheableDependencyInterface &$cacheability) {
  FormHooks::hookMenuLocalTasksAlter($data, $route_name, $cacheability);
}

/**
 * Implements hook_entity_field_access().
 */
function ticketforum_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
  return EntityAccessHooks::entityFieldAccess($operation, $field_definition, $account, $items);
}
