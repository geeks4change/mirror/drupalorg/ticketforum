<?php

namespace Drupal\ticketforum;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

class EntityAccessHooks {

  /**
   * Implements hook_entity_field_access().
   */
  public static function entityFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($field_definition->getTargetEntityTypeId() === 'node' && $operation === 'edit') {
      if ($field_definition->getName() === 'field_taxonomy_ticketforums') {
        return AccessResult::forbiddenIf(!$account->hasPermission('ticketforum move ticketforum topic'))
          ->addCacheContexts(['user.permissions']);
      }
      if ($field_definition->getName() === 'field_ticketforum_tags') {
        return AccessResult::forbiddenIf(!$account->hasPermission('ticketforum tag ticketforum topic'))
          ->addCacheContexts(['user.permissions']);
      }
      if ($field_definition->getName() === 'field_ticketforum_topic_status') {
        return AccessResult::forbiddenIf(!$account->hasPermission('ticketforum change ticketforum topic status'))
          ->addCacheContexts(['user.permissions']);
      }
    }
    return AccessResult::neutral();
  }

}
