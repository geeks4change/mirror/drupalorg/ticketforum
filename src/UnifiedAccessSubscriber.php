<?php

namespace Drupal\ticketforum;

use Drupal\entity_unified_access\Conditions\AndConditionGroup;
use Drupal\entity_unified_access\Conditions\ConstantCondition;
use Drupal\entity_unified_access\Conditions\FieldCondition;
use Drupal\entity_unified_access\Conditions\OrConditionGroup;
use Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UnifiedAccessSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'entity.unified_access.node' => ['unifiedNodeAccess'],
      'entity.unified_access.taxonomy_term' => ['unifiedTermAccess'],
    ];
    return $events;
  }

  /**
   * Unified access subscriber: Terms.
   *
   * @param \Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessEvent $event
   */
  public function unifiedTermAccess(UnifiedAccessEvent $event) {
    $account = $event->getAccount();
    $event->getConditions()
      ->add(OrConditionGroup::create('ticketforum')
        ->add(FieldCondition::create('otherBundlesPassThrough', 'vid', 'ticketforums', '<>'))
        ->add(ConstantCondition::ifPermission('isAdmin', 'administer ticketforums', $account))
        ->add(AndConditionGroup::create('mortalUser')
          ->add(ConstantCondition::ifPermission('hasAccess', 'access ticketforums', $account))
          ->add(OrConditionGroup::create('forumAccess')
            ->add(FieldCondition::create('publicForum', 'field_ticketforum_internal', 'public'))
            ->add(FieldCondition::create('isAssigned', 'field_ticketforum_assigned', $account->id())->addCacheContexts(['user']))
            ->add(FieldCondition::create('isInvolved', 'field_ticketforum_involved', $account->id())->addCacheContexts(['user']))
            ->add(FieldCondition::create('isInterested', 'field_ticketforum_interested', $account->id())->addCacheContexts(['user']))
          )
        )
      );
  }

  /**
   * Unified access subscriber: Nodes.
   *
   * @param \Drupal\entity_unified_access\UnifiedAccess\UnifiedAccessEvent $event
   */
  public function unifiedNodeAccess(UnifiedAccessEvent $event) {
    $account = $event->getAccount();
    $event->getConditions()
      ->add(OrConditionGroup::create('ticketforum')
        ->add(FieldCondition::create('otherBundlesPassThrough', 'type', 'ticketforum', '<>'))
        ->add(ConstantCondition::ifPermission('isAdmin', 'administer ticketforums', $account))
        ->add(AndConditionGroup::create('mortalUser')
          ->add(ConstantCondition::ifPermission('hasAccess', 'access ticketforums', $account))
          ->add(OrConditionGroup::create('forumAccess')
            // ->add(Condition::create('publicForum', 'field_taxonomy_ticketforums.entity.field_ticketforum_internal', 'public')
            ->add(FieldCondition::create('publicForum', 'field_taxonomy_ticketforums', $this->publicForumTermIds())
              ->addCacheableListDependency('taxonomy_term', ['ticketforums']))
            ->add(FieldCondition::create('isAssigned', 'field_ticketforum_assigned', $account->id())->addCacheContexts(['user']))
            ->add(FieldCondition::create('isInvolved', 'field_ticketforum_involved', $account->id())->addCacheContexts(['user']))
            ->add(FieldCondition::create('isInterested', 'field_ticketforum_interested', $account->id())->addCacheContexts(['user']))
          )
        )
      );
  }

  /**
   * Fetch public forum tids.
   *
   * Doe to a current limitation in @see \Drupal\entity_unified_access\QueryAccess\ViewsQueryAlter::mapConditions
   * we can not add relations. So enum the tids here.
   *
   * @return int[]
   */
  private function publicForumTermIds() {
    $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $query = $termStorage->getQuery()
      ->condition('vid', 'ticketforums')
      ->condition('field_ticketforum_internal', 'public', 'STARTS_WITH');
    $ids = $query->execute();
    return $ids;
  }

}
